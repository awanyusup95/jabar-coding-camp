// ----- soal 1  ------------------ Soal No 1  ------------------ Soal No 1  -----------
	console.log("\n -------- Soal 1 ------- ") ;
	// Jawaban soal 1 
	function next_date(p_tanggal, p_bulan, p_tahun) {
 
	    // mun tanggal 31 bulan 1  , tanggal jadi 1 bulan jadi nambah 1  
	    if ((p_tanggal == 31) && p_bulan == 1){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    } 
	    // Mun tanggal 29 pas (kabisat) tanggal jadi 1 bulan nambah 1 
	    else if (p_tanggal == 29 && p_bulan == 2 && p_tahun % 4 == 0) {
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ; 
	    }
	    // Mun tanggal 28 non kabisat tanggal jadi 1 bulan nambah 1 
	    else if (p_tanggal == 28 && p_bulan == 2 && p_tahun % 4 != 0)  {
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ; 
	    }
	    // mun tanggal 31 bulan 3  , tanggal jadi 1 bulan jadi nambah 1  
	    else if ((p_tanggal == 31) && p_bulan == 3){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    } 	    
	    // mun tanggal 30 bulan 4  , tanggal jadi 1 bulan jadi nambah 1  
	    else if ((p_tanggal == 30) && p_bulan == 4){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    } 
	    // mun tanggal 31 bulan 5  , tanggal jadi 1 bulan jadi nambah 1  
	    else if ((p_tanggal == 31) && p_bulan == 5){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    }
	    // mun tanggal 30 bulan 6  , tanggal jadi 1 bulan jadi nambah 1	     	    
	    else if ((p_tanggal == 30) && p_bulan == 6){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    }
	    // mun tanggal 31 bulan 7  , tanggal jadi 1 bulan jadi nambah 1	     	    
	    else if ((p_tanggal == 31) && p_bulan == 7){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    }
	    // mun tanggal 31 bulan 8  , tanggal jadi 1 bulan jadi nambah 1	     	    
	    else if ((p_tanggal == 31) && p_bulan == 8){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    }
	    // mun tanggal 30 bulan 9  , tanggal jadi 1 bulan jadi nambah 1	     	    
	    else if ((p_tanggal == 30) && p_bulan == 9){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    }	    
	    // mun tanggal 31 bulan 10  , tanggal jadi 1 bulan jadi nambah 1	     	    
	    else if ((p_tanggal == 31) && p_bulan == 10){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    }
	    // mun tanggal 30 bulan 11  , tanggal jadi 1 bulan jadi nambah 1	     	    
	    else if ((p_tanggal == 30) && p_bulan == 11){
	    	p_tanggal = 1 ; 
	    	p_bulan = p_bulan +1 ;
	    }
	    // mun tanggal 31 bulan 12  , tanggal jadi 1 bulan jadi 1 tahun nambah 1	     	    
	    else if ((p_tanggal == 31) && p_bulan == 12){
	    	p_tanggal = 1 ; 
	    	p_bulan = 1 ;
	    	p_tahun = p_tahun +1 ;
	    }

	    // sisana kantun nambih tanggal hungkul 
	    else {
	    	p_tanggal = p_tanggal + 1 ;
	    }

	    // Ubah nomor bulan menjadi nama bulan
	    var hasil_bulan; 
		switch (p_bulan){
	        case 1: {hasil_bulan="Januari"; break;}
	        case 2: {hasil_bulan="Februari"; break;}
	        case 3: {hasil_bulan="Maret"; break;}
	        case 4: {hasil_bulan="April"; break;}
	        case 5: {hasil_bulan="Mei"; break;}
	        case 6: {hasil_bulan="Juni"; break;}
	        case 7: {hasil_bulan="Juli"; break;}
	        case 8: {hasil_bulan="Agustus"; break;}
	        case 9: {hasil_bulan="September"; break;}
	        case 10: {hasil_bulan="Oktober"; break;}
	        case 11: {hasil_bulan="November"; break;}
	        case 12: {hasil_bulan="Desember"; break;}
	        default: { hasil_bulan="Angka Bulan Salah !!!";}
	    }

	    return console.log(p_tanggal+" "+hasil_bulan+" "+p_tahun) ;
	}

	var tanggal = 29 ;
	var bulan = 2 ; 
	var tahun = 2020 ; 

	console.log("\n  Contoh 1 : ");
	next_date(tanggal , bulan , tahun ) ; 
	console.log("\n  Contoh 2 : ");
	next_date(28 , 2 , 2021) ; 
	console.log("\n  Contoh 3 : ");
	next_date(31 , 12 , 2020 ) ;   


// ----- soal 2  ------------------ Soal No 2  ------------------ Soal No 2  -----------
	console.log("\n -------- Soal 2 ------- ") ;
	// Jawaban soal 2 
	function jumlah_kata(p_kalimat) {

 		var arr_kalimat = p_kalimat.split(" ");  
 		let arr_bersih=[] ;  
 		for (var i = 0 ; i <= arr_kalimat.length; i++) {
 		 	if (arr_kalimat[i] != ''){ 
 		 		arr_bersih.push(arr_kalimat[i]);
 		 	}  
 		}  
 		return console.log(arr_bersih.length-1);   
	} 
	var kalimat_1 =  " Halo nama saya Muhammad Iqbal Mubarok " ;
	var kalimat_2 = " Saya Iqbal";
	var kalimat_3 = " Saya Muhammad Iqbal Mubarok ";

	jumlah_kata(kalimat_1); // 6
	jumlah_kata(kalimat_2) ;// 2
	jumlah_kata(kalimat_3); // 4
