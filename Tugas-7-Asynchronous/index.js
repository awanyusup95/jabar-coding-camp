// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
readBooks(10000,books[0],(callback) => {  console.log(callback) })
readBooks(7000,books[1],(callback) => {  console.log(callback) })
readBooks(5000,books[2],(callback) => {  console.log(callback) })
readBooks(1000,books[3],(callback) => {  console.log(callback) }) 
 
 
// books.forEach(element => readBooks(10000, element, (callbackFn) => {  console.log(callbackFn) }))

