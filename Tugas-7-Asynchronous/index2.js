var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000} 
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
const init = async() => { 
        await readBooksPromise(10000,books[0],(callback) => {  console.log(callback) })
        await readBooksPromise(7000,books[1],(callback) => {  console.log(callback) })
        await readBooksPromise(5000,books[2],(callback) => {  console.log(callback) })
        await readBooksPromise(1000,books[3],(callback) => {  console.log(callback) })
}
init()
