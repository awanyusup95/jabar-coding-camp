// ----- Soal 1 ----- 
console.log("----- Soal 1 -----") ; 
	// Jawaban soal 1 
 	const luas = (panjang, lebar) => { 
 		return panjang * lebar ; 
 	}
 	const keliling = (panjang, lebar) => { 
 		return ((panjang*2)+(lebar * 2));
 	}

	console.log("Maka Luasnya adalah = "+ luas(5,4)) ; 
	console.log("Maka Kelilingnya adalah = "+ keliling(5,4)) ;
	console.log("") ; 

// ----- Soal 2 ----- 
console.log("----- Soal 2 -----") ; 
	// Jawaban soal 2 
 	const newFunction = (firstName, lastName) => {
	  return {
	    firstName: firstName,
	    lastName: lastName,
	    fullName: function(){
	      console.log(firstName + " " + lastName);
	    }
	  }
	}
 
	//Driver Code 
	newFunction("William", "Imoh").fullName() ;
	console.log("") ;


// ----- Soal 3 ----- 
console.log("----- Soal 3 -----") ; 
	// Jawaban soal 3 	
	const newObject = {
	  firstName: "Muhammad",
	  lastName: "Iqbal Mubarok",
	  address: "Jalan Ranamanyar",
	  hobby: "playing football",
	}

	const {firstName, lastName, address, hobby} = newObject;

	// Driver code
	console.log(firstName, lastName, address, hobby);
	console.log("") ;


// ----- Soal 4 ----- 
console.log("----- Soal 4 -----") ; 
	// Jawaban soal 4 	

	const west = ["Will", "Chris", "Sam", "Holly"] ;
	const east = ["Gill", "Brian", "Noel", "Maggie"] ;
	const combined = [...west,...east];
	//Driver Code
	console.log(combined); 
	console.log("") ;



// ----- Soal 5 ----- 
console.log("----- Soal 5 -----") ; 
	// Jawaban soal 5

	const planet = "earth" ;
	const view = "glass" ;
	const before = 'Lorem ' + view + ' dolor sit amet, ' + 'consectetur adipiscing elit ,' + planet ; 
	const gabungan =  `${before} ${view} ${planet}`; 
	console.log(gabungan) ; 

