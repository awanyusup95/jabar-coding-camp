//Tugas 5 
//Soal 1 
console.log("-------  Soal 1  -------")
	// Jawaban soal 1
	// Membuat dan menampilkan array awal  
	var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular",  "1. Tokek"]; 

	// Mengurutkan isi array sesuai dengan abjad 
	var terurut = daftarHewan.sort();
	console.log(" Isi Variabel Setelah terurut  : ");
	
	// Menghitung panjang array
	var panjang_array =  terurut.length; 
	
	// Membuat looping untuk menampilkan isi array 
	for (var i = 0; i < panjang_array; i++) {
		console.log("   "+terurut[i]);
	} 


//Soal 2
console.log("");
console.log("-------  Soal 2  -------"); 
	// Jawaban soal 2
	// membuat funsi introduce 
	function introduce(datanya){
		var kalimat ="Nama saya "+datanya.name+", Umur saya "+datanya.age+" tahun, alamat saya di "+datanya.address+" dan saya punya hobby yaitu "+datanya.hobby ;
		return (kalimat);
	}
 	// membuat variabel untuk menampung 
	var data = {name : "Ryan Yusup Hendriawan" , age : 23 , address : "Jalan Panorama Lembang" , hobby : "Badminton" } ;
 
 	// membuat var baru dan memanggil fungsi introduce dengan parameternya yaitu var data
	var perkenalan = introduce(data) ;
	console.log(perkenalan);


//Soal 3
console.log("");
console.log("-------  Soal 3  -------"); 
	// Jawaban soal 3
	function hitung_huruf_vokal(par_kalimat){
		var kalimat = par_kalimat.toLowerCase() ;   
		panjang_karakter = kalimat.length; 
		var hasil_hitung = 0;
		for (var i = 0; i <= panjang_karakter; i++) { 
			if ( 	(kalimat.charAt(i) == 'a') ||
					(kalimat.charAt(i) == 'i') ||
					(kalimat.charAt(i) == 'u') || 
					(kalimat.charAt(i) == 'e') ||
					(kalimat.charAt(i) == 'o') 
			 	){
				hasil_hitung++;
			}
		}
		return hasil_hitung;
	}

	var hitung_1 = hitung_huruf_vokal("Muhammad");
	var hitung_2 = hitung_huruf_vokal("Iqbal");
	console.log(hitung_1 , hitung_2);

//Soal 4
console.log("");
console.log("-------  Soal 4  -------"); 
	// Jawaban soal 4
	function hitung(bilangan){
		var keluaran = (bilangan *2) - 2 ; 
		return keluaran;
	} 

	console.log( hitung(0) ); // -2
	console.log( hitung(1) ); // 0
	console.log( hitung(2) );// 2
	console.log( hitung(3) ); // 4
	console.log( hitung(5) ); // 8 