//Soal 1 
console.log("-----  Soal 1  -----")
	// Jawaban soal 1
	var pertama = "saya sangat senang hari ini";
	var kedua ="belajar javascript itu keren";

	var kata_saya = pertama.substring(0,4);
	var kata_senang = pertama.substring(12,18); 
	var kata_belajar = kedua.substr(0,7); 
	var kata_javascript = kedua.substr(8,10).toUpperCase();  

	var gabung = kata_saya.concat(" ",kata_senang," ",kata_belajar, " ", kata_javascript)
	console.log(gabung); 
	console.log("");

//Soal 2
console.log("-----  Soal 2  -----")
	// Jawaban soal 2
	var kataPertama = "10";
	var kataKedua = "2";
	var kataKetiga = "4";
	var kataKeempat = "6";
	
	var num_pertama = Number(kataPertama); 
	var num_kedua = Number(kataKedua); 
	var num_ketiga = Number(kataKetiga); 
	var num_keempat = Number(kataKeempat); 
	var hasil = (num_pertama % num_kedua) + ( num_ketiga * num_keempat)  
	console.log("Persamaan 1 :");
	console.log('  (10 mod 2)+(6 x 4) = '+ hasil);
	console.log("Persamaan 2 :  ");
	var hasil2 = (num_pertama+num_keempat-num_ketiga)*num_kedua
	console.log('  (10+6-4)*2 = '+ hasil2);
	console.log("");
	
//Soal 3
console.log("-----  Soal 3  -----")
	// Jawaban soal 3
	var kalimat = 'wah javascript itu keren sekali'; 

	var kataPertama = kalimat.substring(0, 3); 
	var kataKedua = kalimat.substring(4, 14);  
	var kataKetiga= kalimat.substring(15, 18); 
	var kataKeempat= kalimat.substring(19, 25);   
	var kataKelima= kalimat.substring(25, 32);  

	console.log('Kata Pertama: ' + kataPertama); 
	console.log('Kata Kedua: ' + kataKedua); 
	console.log('Kata Ketiga: ' + kataKetiga); 
	console.log('Kata Keempat: ' + kataKeempat); 
	console.log('Kata Kelima: ' + kataKelima);
		
	