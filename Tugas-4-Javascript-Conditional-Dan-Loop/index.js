//Soal 1 
console.log("-------  Soal 1  -------")
	// Jawaban soal 1
	var nilaiku = 68 ; 
	var indeks;
	console.log("  Nilai ku = "+nilaiku); 

	if (nilaiku >= 85){
		indeks='A';
	}else if (nilaiku >= 75){
		indeks='B';
	}else if (nilaiku >= 65){
		indeks='C';
	}else if (nilaiku >= 55){
		indeks='D';
	}else {
		indeks='E';
	}  
	console.log("  Indeks ku = "+indeks); 
	console.log(""); 

//Soal 2
console.log("-------  Soal 2  -------")
	// Jawaban soal 2	
	var tanggal = 1;
	var bulan = 10;
	var tahun = 1998;
	console.log("Tanggal : "+tanggal);
	console.log("Bulan   : "+bulan);
	console.log("Tahun   : "+tahun);console.log("");

	switch (bulan){
		case 1: { console.log(tanggal+" Januari "+tahun);  break;}
		case 2: { console.log(tanggal+" Februari "+tahun);  break;}
		case 3: { console.log(tanggal+" Maret "+tahun);  break;}
		case 4: { console.log(tanggal+" April "+tahun);  break;}
		case 5: { console.log(tanggal+" Mei "+tahun);  break;}
		case 6: { console.log(tanggal+" Juni "+tahun);  break;}
		case 7: { console.log(tanggal+" Juli "+tahun);  break;}
		case 8: { console.log(tanggal+" Agustus "+tahun);  break;}
		case 9: { console.log(tanggal+" September "+tahun); break;}
		case 10: { console.log(tanggal+" Oktober "+tahun);  break;}
		case 11: { console.log(tanggal+" November "+tahun);  break;}
		case 12: { console.log(tanggal+" Desember "+tahun);  break;}
		default: { console.log(" Isi Variabel bulan harus lah 1,2,3,4,5,6,7,8,9,10,11,12  !!! ");}
	}

//Soal 3
console.log("");
console.log("-------  Soal 3  -------")
	// Jawaban soal 3
	var pagar="";
	var n = 7;
	console.log(" Tinggi/Alas : "+n); 

	for (var i = 1; i <= n; i++) {  
			pagar += "#" ;
			console.log(pagar);  
	}


//Soal 4
console.log("");
console.log("-------  Soal 4  -------")
	// Jawaban soal 4
	var m = 11; 
	var samdeg="";
	console.log("Nilai m = "+m);

	for (var i = 1; i <= m; i++) {
		samdeg += "=" ;
		if (i % 3 == 1 ) {
			console.log(i+" I Love programming ");	
		}else if (i % 3 == 2 ){
			console.log(i+" I Love Javascript ");	
		}else if (i % 3 == 0 ){
			console.log(i+" I Love VueJS ");
			console.log(samdeg);	
		} 
		  
	}
		
